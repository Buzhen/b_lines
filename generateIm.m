function im = generateIm()

dbstop error;
line_thickness_min = 10;
line_thickness_max = 20;
horz_line_ang_range_deg = 30;
vert_line_ang_range_deg = 50;
min_b_line_num = 3;
max_b_line_num = 7;

imSize = [600,600];
r_max = sqrt(sum(imSize(:).^2));

% Lines coordinates are given as [r, theta, thickness] where the origin is
% the image center
pluelar_line_coords = [imSize(1)/3 * rand() + imSize(1)/6, ...
    90+(randn()*horz_line_ang_range_deg/4), ...
    randi(line_thickness_max-line_thickness_min)+line_thickness_min];
B_line_num = randi(max_b_line_num-min_b_line_num)+min_b_line_num;
b_lines = zeros(B_line_num, 3);
for b_line_I = 1:B_line_num
    b_lines(b_line_I,:) = [imSize(2)/2*rand(), ...
        mod(randn()*vert_line_ang_range_deg/2, 180), ...
    randi(line_thickness_max-line_thickness_min)+line_thickness_min];
end

[X, Y] = meshgrid(1:imSize(1), 1:imSize(2));
im_coords = [X(:), Y(:)]'-(imSize'+1)/2;
lines = [pluelar_line_coords; b_lines];
Is = false(1,length(X(:)));
for lineI = 1:size(lines,1)
    line = lines(lineI,:);
    IsI = abs(sum(im_coords.*[cosd(line(2)); sind(line(2))]) - line(1)) ...
        < line(3)/2;
    fprintf(1,'%d\n', sum(IsI));
    Is = Is | IsI;
    if lineI==1
        Is_above_pluelar = sum(im_coords.*[cosd(line(2)); sind(line(2))]) - line(1) > line(3)/2;
    end
end
Is(Is_above_pluelar) = 0;
im = zeros(imSize(1), imSize(2));
im(Is) = 1;
figure(1); imshow(im); colormap gray; axis image xy;

gauss = gen_gaussian_image(600,600,35,35);
im_blur = abs(ifft2(fftshift(gauss.*fftshift(fft2(im)))));
im_blur = im_blur./(max(im_blur(:)))*0.6;
im2 = max(im_blur,0.5);
figure(2); imagesc(im2); colormap gray; axis image xy;
figure(3); imagesc(imnoise(im2, 'speckle', 0.03)); axis xy image; colormap gray;
im_blur2 = abs(ifft2(fftshift(gauss.*fftshift(fft2(imnoise(im2, 'speckle', 0.03))))));
im_blur2 = im_blur2./(max(im_blur2(:)));
figure(4); imagesc(im_blur2); axis xy image; colormap gray;