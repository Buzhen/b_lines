function gaussian_im = gen_gaussian_image(n, m, sigma_x, sigma_y)

[X, Y] = meshgrid(1:m, 1:n);
coords = [X(:), Y(:)]';
center = [m/2; n/2];

im = 1/(2*pi*sigma_x*sigma_y) * ...
    exp(-sum((coords-center).^2/2./[sigma_x^2; sigma_y^2]));
gaussian_im = reshape(im, n, m);